$(document).ready(function () {

    var articleEditors = document.getElementsByClassName("articleEditor");

   // for loop for all the articles to embedd wysiwyg editor
   for (var i = 0; i < articleEditors.length; i++) {
    ClassicEditor
        .create(articleEditors[i], {

           // plugins: [Bold, Italic, Underline, Strikethrough, Code],
           toolbar: [ 'heading', '|', 'bold', 'italic', 'link', 'bulletedList', 'numberedList', 'blockQuote' ],

        })
        .catch(error => {
            console.error(error);
        });
    }
    
});


// Open and CLose the side navigation menu 
function openNav() {
    document.getElementById("mySidenav").style.width = "200px";
    document.getElementById("main").style.marginLeft = "200px";

};

function closeNav() {
    document.getElementById("mySidenav").style.width = "0px";
    document.getElementById("main").style.marginLeft = "0px";

};

// Fade in the avatars when the edit button is clicked in the editProfile page
function showAvatars() {
    $('#profileCurrentAvatar').fadeOut(1);
    $('.avatarSelection').fadeIn(500);
};


// Check with backend database for existing users when new user is signing up
function checkUser() {
    $.ajax({
        url: "http://localhost:3000/profile/checkuser",
        type: 'GET',
        dataType: 'JSON',
        data: {
            username: $('#newUsername').val()
        },
        success: function (data) {
            if (data.success) {
                console.log("ajax success")
                if (!data.username) {
                    $('#usernameErrorMsg').text("");
                } else {
                    $('#usernameErrorMsg').text("That username is not available");
                    $('#usernameErrorMsg').css("color", "red");
                }
            } else if (data.username) {
                console.log("ajax failed")
                $('#usernameErrorMsg').text("That username is acceptable");
                $('#usernameErrorMsg').css("color", "green");
            }
        }
    });
}


// Validation check for the password field to check they match. See html validation for the format validation
function checkPass() {
    var pass1 = $("#psw");
    var pass2 = $("#psw-repeat");
    if (pass1.val() != pass2.val()) {
        $("#passwordErrorMsg").text("Passwords don't match");
        $("#passwordErrorMsg").css("color", "red");
        $("#submitBtn").attr("disabled", true);
    } else {
        $("#passwordErrorMsg").text("Password looks great");
        $("#passwordErrorMsg").css("color", "green");
        $("#submitBtn").attr("disabled", false);

    }
}




