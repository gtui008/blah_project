// Allow us to use SQLite3 from node.js
var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database('blah.db');
db.get("PRAGMA foreign_keys = ON");


module.exports.getUser = function (username, callback) {
    db.all('SELECT * FROM users WHERE username = ?', [username], function (err, rows) {
        if (rows.length > 0) {
            callback(rows[0]);
        } else {
            callback(null);
        }
    });
};

// A function which loads all articles from the database, then sends them to the given callback.
module.exports.getFullArticles = function (callback) {
    db.all("SELECT articles.*, users.fname, users.lname, (SELECT distinct COUNT(*) FROM comments WHERE comments.articleID = articles.articleID) AS Comment_Count FROM articles, users WHERE users.username = articles.username ORDER BY articleID desc", function (err, rows) {

        
        // console.log(rows);

        // "rows" is a JavaScript array, and we can do anything we would normally do for such an array.
        callback(rows);
    });
};


// A function which loads all articles from the database, which belongs to the specified users and then sends them to the given callback.
module.exports.getAllUserArticles = function (username, callback) {
    db.all("SELECT * FROM articles WHERE username = ? ORDER BY articleID desc", [username], function (err, rows) {
        callback(rows);
    });
};

// A function which gets the article with the given id from the database, then sends it to the given callback.
module.exports.getArticle = function (id, callback) {
    db.all("SELECT * FROM articles WHERE articleID = ?", [id], function (err, rows) {

        var article = rows[0]; // There should only be one...

        callback(article);
    });
};

// A function that adds a new article with the given title and content to the database, then sends its automatically generated id to the given callback.
module.exports.addArticle = function (title, content, username, callback) {

    db.run("INSERT INTO articles (title, content, username, datePosted) VALUES (?,?,?,?)", [title, content, username, timeStamp()], function (err) {
        callback(this.lastID);
    });
};

module.exports.editArticle = function (articleTitle, articleContent, articleID, callback) {
    db.run("UPDATE articles SET title = ?, content = ? WHERE articleID = ?", [articleTitle, articleContent, articleID], function (err) {
        console.log(err);
        callback(this.lastID);
    });
};
// A function that deletes the article with the given id from the database, then sends the number of rows affected (probably 0 or 1) to the given callback.
module.exports.deleteArticle = function (id, callback) {
    console.log("Article to delete is" + id);
    db.run("DELETE FROM articles WHERE articleID = ?", [id], function (err) {

        callback(this.changes);

    });
};

module.exports.addUser = function (username, password, fname, lname, dob, country, avatar, callback) {
    db.run("INSERT INTO users (username,password,fname, lname, dob, country, avatar) VALUES (?,?,?,?,?,?,?)", [username, password, fname, lname, dob, country, avatar], function (err) {

        if (err) console.log(err);
        callback("User added");

    });
};

module.exports.changeUserPassword = function (username, hash,callback){
    db.run("UPDATE users SET password = ? WHERE username = ?",[hash,username], function(err){
        if(err) console.log("error changing password " + err);
        callback('password updated');
    });
}

module.exports.deleteUser = function (username, callback) {
    db.run("DELETE from users where username = ?", [username], function (err) {

        if (err) console.log(err);
        callback(this.changes);

    });
};

module.exports.updateUser = function (fname, lname, dob, country, description, avatar, username, callback) {
    db.run("UPDATE users SET fname = ?, lname = ?, dob = ?, country = ?, description = ?, avatar = ? WHERE username = ?",
        [fname, lname, dob, country, description, avatar, username],
        function (err) {

            if (err) console.log(err);
            callback("User updated");
        });

};

module.exports.updateUserNoAvatar = function (fname, lname, dob, country, description, username, callback) {
    db.run("UPDATE users SET fname = ?, lname = ?, dob = ?, country = ?, description = ? WHERE username = ?",
        [fname, lname, dob, country, description, username],
        function (err) {

            if (err) console.log(err);
            callback("User updated");
        });

}

module.exports.addComment = function (username, articleID, comment, callback) {
    db.run("INSERT INTO comments (username,articleID,content, datePosted) values (?,?,?,?)", [username, articleID, comment, timeStamp()], function (err) {
        callback(this.lastID);
    });
};

module.exports.deleteComment = function (commentID, callback) {
    db.run("DELETE from comments WHERE id = ?", [commentID], function (err) {
        callback(this.changes);

    });

};

module.exports.getCommentsForArticle = function (articleID, callback) {
    db.all("select c.*, u.avatar, u.fname, u.lname from comments c, users u where c.username = u.username AND c.articleID = ?", [articleID], function (err, rows) {
        callback(rows);
    });

};

module.exports.getAllComments = function (callback) {
    db.all("SELECT c.*, a.title from comments c, articles a where c.articleID = a.articleID", function (err, rows) {
        callback(rows);
    });
};

module.exports.getAllCommentsFor = function (username, callback) {
    db.all("SELECT * from comments where userid = ?", [username], function (err, rows) {
        callback(rows);
    });
};


module.exports.getAllUserComments = function (username, callback) {
    db.all("SELECT c.*, a.title from comments c, articles a where c.articleID = a.articleID AND c.username = ?", [username], function (err, rows) {
        callback(rows);
    });
};

function timeStamp() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();

    if (dd < 10) dd = '0' + dd
    
    if (mm < 10)  mm = '0' + mm

    console.log(dd + '/' + mm + '/' + yyyy) ;

    return dd + '/' + mm + '/' + yyyy;
}