// -------------------------------------------------------------------------
// Setup Express / Handlebars / Body-Parser / fs / cookie-parser / express-session 
// -------------------------------------------------------------------------
var express = require("express");
var app = express();
app.set("port", process.env.PORT || 3000);

var handlebars = require("express-handlebars");
app.engine("handlebars", handlebars({
  defaultLayout: "main-layout"
}));
app.set("view engine", "handlebars");

// Add "fs" module
var fs = require("fs");

// Specify that the app should use body parser (for reading submitted form POST data)
var bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({
  extended: true
}));

// User cookie-parser for our cookies
var cookieParser = require("cookie-parser");
app.use(cookieParser());

// Specify that the app should use express-session to create in-memory sessions
var session = require("express-session");
app.use(
  session({
    resave: false,
    saveUninitialized: false,
    secret: "compsci719"
  })
);

module.exports.session = session;

var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;

// Function to get the specified user from the Database
var findUser = function (username, callback) {
  username = username.toLowerCase();
  DAO.getUser(username, function (user) {
    if (user) {
      session["avatar"] = user.avatar; // store the users avatar in the session
    }
    callback(user);
  });
};

var localStrategy = new LocalStrategy(
  function (username, password, done) { // username and password from login form

    // Get the user from the "database"
    findUser(username, function (user) {

      // If the user doesn't exist...
      if (!user) {
        return done(null, false, {
          message: 'Invalid user'
        });
      };

      bcrypt.compare(password, user.password, function (err, res) {

        if (!res) {
          return done(null, false, {
            message: 'Invalid password'
          });
        }

        done(null, user);

      });

    });
  }
);

// This method will be called when we need to save the currently
// authenticated user's username to the session.
passport.serializeUser(function (user, done) {
  done(null, user.username);
});

// This method will be called when we need to get all the data relating
// to the user with the given username.
passport.deserializeUser(function (username, done) {
  findUser(username, function (user) {
    done(null, user);
  });
});

// Set up Passport to use the given local authentication strategy
// we've defined above.
passport.use('local', localStrategy);

// Start up Passport, and tell it to use sessions to store necessary data.
app.use(passport.initialize());
app.use(passport.session());

// Use our custom DAO module
var DAO = require('./dao-module.js');

// Bycrypt required for password encryption
var bcrypt = require('bcrypt');
const saltRounds = 10;


// -------------------------------------------------------------------------
// HELPER FUNCTIONS
// -------------------------------------------------------------------------
// Helper function to make sure that the returned result is an array.
function ensureArray(value) {
  if (value == undefined) return [];
  else return Array.isArray(value) ? value : [value];
}

function checkArticleOwner(articles, loggedInUser) {
  var articlesArray = ensureArray(articles);
  for (var i = 0; i < articlesArray.length; i++) {

    if (articlesArray[i].username == loggedInUser.username) {
      articlesArray[i]["isOwner"] = true;
    }

  }
}

function checkCommentOwner(comments, loggedInUser) {
  for (var i = 0; i < comments.length; i++) {

    if (comments[i].username == loggedInUser.username) {
      comments[i]["isOwner"] = true;
    }

  }
}

function createArticleSamples(articles) {
  for (var i = 0; i < articles.length; i++) {
    let sample = articles[i].content.replace(/<[^>]*>/g, ''); // Get pure text only without the html 
    articles[i].sample = sample.substring(0, 200); // Create sample with the first 200 characters 
  }
}

module.exports.checkArticleOwner = checkArticleOwner;
module.exports.checkCommentOwner = checkCommentOwner;

// -------------------------------------------------------------------------
// FUNCTIONS END
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// ROUTE HANDLERS
// -------------------------------------------------------------------------

var profile = require('./routes/profile');
var article = require('./routes/article');

app.use('/profile', profile);
app.use('/article', article);

app.get('/', function (req, res) {

  let _articles;
  let defaultAvatars = fs.readdirSync('./public/images/defaultAvatars/');

  DAO.getFullArticles(function (articles) {

    _articles = articles;

    createArticleSamples(_articles);

    if (req.isAuthenticated()) {


      DAO.getUser(req.user.username, function (loggedInUser) {

        checkArticleOwner(_articles, loggedInUser);

        res.render("home", {
          loggedInUser: loggedInUser,
          articles: _articles
        });

      });

    } else {
      res.render("home", {
        articles: _articles,
        defaultAvatars: defaultAvatars
      });
    }
  });

});

app.post('/login', passport.authenticate('local', {
  successRedirect: 'back',
  failureRedirect: '/'
}));

app.get('/about', (req,res) => {
  res.render('about');
})


// Serve "public" files
app.use(express.static(__dirname + "/public"));

// -------------------------------------------------------------------------
// ERROR HANDLERS
// -------------------------------------------------------------------------

//Display 404 error for any issues exist in client side

app.use(function (req, res, next) {
  res.type("text/html");
  res.status(404);
  res.render('404-error');
  //res.sendfile(__dirname + "/public/404-error-page.html");
});

// Handle 500 error
app.use(function (err, req, res, next) {
  console.error(err);
  res.status(500);
  res.render('server-error', {
    title: '500: Internal Server Error',
    error: err
  });
});

// Start the server running.
app.listen(app.get("port"), function () {
  console.log("Express started on http://localhost:" + app.get("port"));
});