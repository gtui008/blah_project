const express = require('express');
const profileRouter = express.Router();
const DAO = require('../dao-module');
const fs = require("fs");
var formidable = require("formidable");
var jimp = require("jimp");
var bcrypt = require('bcrypt');
const saltRounds = 10;

profileRouter.get('/', (req, res) => {

    if (req.isAuthenticated()) {
        DAO.getUser(req.user.username, function (loggedInUser) {

            // let _loggedInUser = loggedInUser;

            DAO.getAllUserArticles(req.user.username, function (articles) {

                DAO.getAllUserComments(req.user.username, function (comments) {

                    createArticleSamples(articles);

                    res.render('profilePage', {
                        loggedInUser: loggedInUser,
                        articles: articles,
                        comments: comments
                    });

                });

            });

        });

    } else {
        res.redirect('/');
    }

});

profileRouter.get("/logout", function (req, res) {
    req.logout();
    res.redirect('back');
});

profileRouter.get('/edit', (req, res) => {
    let defaultAvatars = fs.readdirSync(__dirname + '/../public/images/defaultAvatars/');

    if (req.isAuthenticated()) {

        DAO.getUser(req.user.username, function (loggedInUser) {

            DAO.getAllUserArticles(req.user.username, function (articles) {

                res.render('editProfilePage', {
                    loggedInUser: loggedInUser,
                    articles: articles,
                    defaultAvatars: defaultAvatars
                })
            });

        });

    } else {
        res.redirect('/');
    }

});

profileRouter.post('/edit', (req, res, next) => {
    if (!req.isAuthenticated()) {
        res.redirect('/');

    } else {
        var form = new formidable.IncomingForm();
        form.on("fileBegin", function (name, file) {
            if (file.name) {
                file.path = __dirname + "/../public/images/customAvatars/" + file.name;
            }
        });

        form.parse(req, (err, fields, files) => {
            var username = req.user.username;
            var fname = fields.fname;
            var lname = fields.lname;
            var dob = fields.dob;
            var country = fields.country;
            var description = fields.description;
            var avatar = fields.avatar;

            if (validateUsername(username) == false || validateName(fname) == false || validateName(lname) == false || validateDOB(dob) == false || validateCountry(country) == false || validateDescription(description) == false) {
                console.log("validateUsername " + validateUsername(username));
                console.log("validateName " + validateName(fname));
                console.log("validateName " + validateName(lname));
                console.log("validateDOB " + validateDOB(dob));
                console.log("validateCountry " + validateCountry(country));
                console.log("validateDescription " + validateDescription(description));
                console.log("one of the conditions evaluated to false so no user will not be updated");
                err = new Error("Invalid User Edit Attempt. Malicious attack detected. This event will be reported.");
                next(err);
            } else {


                // If there's a file being uploaded then use this
                if (files.avatarUpload.name) {
                    var uploadedAvatar = files.avatarUpload.name;
                    jimp.read(__dirname + "/../public/images/customAvatars/" + uploadedAvatar, function (err, image) {
                        // the desired size and then save it to the output folder.
                        image.scaleToFit(400, 400).write(__dirname + "/../public/images/customAvatars/" + username + "Avatar." + uploadedAvatar.split('.').pop(), function (err) {

                            // When the image is done saving, print this message to the console
                            if (err) {
                                console.error(
                                    "Error saving file " + uploadedAvatar + "!"
                                );
                            } else {
                                console.log(uploadedAvatar + " saved successfully!");
                                fs.unlinkSync(__dirname + "/../public/images/customAvatars/" + uploadedAvatar); // delete the full size 

                                // Add the user to the database 
                                DAO.updateUser(fname, lname, dob, country, description, "/images/customAvatars/" + username + "Avatar." + uploadedAvatar.split('.').pop(), username, function (callback) {

                                    console.log("New User added with id = " + callback);
                                    res.redirect("/profile");
                                });

                            }
                        });
                    });

                } else if (avatar) { // Otherwise, use default avatar
                    DAO.updateUser(fname, lname, dob, country, description, "/images/defaultAvatars/" + avatar, username, function (callback) {

                        console.log("New User added with id = " + callback);
                        res.redirect("/profile");

                    });

                } else { // If no avatar selected, don't update it.
                    DAO.updateUserNoAvatar(fname, lname, dob, country, description, username, function (callback) {
                        console.log("New User added with id = " + callback);
                        res.redirect("/profile");

                    });
                }
            }
        });
    }

});
profileRouter.post('/changePassword', (req, res) => {
    if (req.isAuthenticated() && validatePassword(req.body.currentPsw) && validatePassword(req.body.psw)) {
        DAO.getUser(req.user.username, function (user) {
            bcrypt.compare(req.body.currentPsw, user.password, function (err, res2) {
                console.log("current" + user.password);
                console.log("entered" + req.body.currentPsw);
                if (res2) {
                    bcrypt.hash(req.body.psw, saltRounds, function (err, hash) {
                        DAO.changeUserPassword(req.user.username, hash, function (callback) {

                            console.log(callback);
                            res.redirect("/profile/logout");


                        });
                    });

                }
            });
        });
    } else {
        res.redirect('/');
    }
});




profileRouter.get('/deleteAccount', (req, res) => {

    if (req.isAuthenticated()) {
        var username = req.user.username;
    }
    DAO.deleteUser(username, function (result) {

        console.log(result)

        req.logout();
        res.redirect('/');

    });

});

profileRouter.post('/signup', (req, res, next) => {

    var form = new formidable.IncomingForm();
    form.on("fileBegin", function (name, file) {
        if (file.name) {
            file.path = __dirname + "/../public/images/customAvatars/" + file.name;
        }
    });

    form.parse(req, (err, fields, files) => {
        let username = fields.newUsername;
        var password = fields.psw;
        var fname = fields.fname;
        var lname = fields.lname;
        var dob = fields.dob;
        var country = fields.country;
        let avatar = fields.avatar;
        let description = fields.description;
        var uploadedAvatar = files.avatarUpload.name;

        if (validateUsername(username) == false || validateName(fname) == false || validateName(lname) == false || validateDOB(dob) == false || validateCountry(country) == false || validatePassword(password) == false || validateDescription(description) == false) {

            console.log("validateUsername " + validateUsername(username));
            console.log("validateName " + validateName(fname));
            console.log("validateName " + validateName(lname));
            console.log("validateDOB " + validateDOB(dob));
            console.log("validateCountry " + validateCountry(country));
            console.log("validatePassword " + validatePassword(password));
            console.log("validateDescription " + validateDescription(description));
            console.log("one of the conditions evaluated to false so no user will be created");

            err = new Error("Invalid User Creation Attempt, Malicious attack detected. This event will be reported.");
            console.log(err);
            next(err);

        } else {

            if (uploadedAvatar) {
                jimp.read(__dirname + "/../public/images/customAvatars/" + uploadedAvatar, function (err, image) {
                    // the desired size and then save it to the output folder.
                    image.scaleToFit(400, 400).write(__dirname + "/../public/images/customAvatars/" + username + "Avatar." + uploadedAvatar.split('.').pop(), function (err) {

                        // When the image is done saving, print this message to the console
                        if (err) {
                            console.error(
                                "Error saving file " + uploadedAvatar + "!"
                            );
                        } else {
                            console.log(uploadedAvatar + " saved successfully!");
                            fs.unlinkSync(__dirname + "/../public/images/customAvatars/" + uploadedAvatar); // delete the full size 


                            // Add the user to the database 
                            bcrypt.hash(password, saltRounds, function (err, hash) {
                                DAO.addUser(username, hash, fname, lname, dob, country, "/images/customAvatars/" + username + "Avatar." + uploadedAvatar.split('.').pop(), function (callback) {

                                    console.log("New User added with id = " + callback);
                                    res.redirect("/");
                                });
                            });
                        }
                    });
                });

            } else { // Otherwise, use default avatar
                bcrypt.hash(password, saltRounds, function (err, hash) {
                    DAO.addUser(username, hash, fname, lname, dob, country, "/images/defaultAvatars/" + avatar, function (callback) {

                        console.log("New User added with id = " + callback);
                        res.redirect("/");

                    });
                });
            }

        }

    });

});

// verify a username exists
profileRouter.get('/checkuser', function (req, res) {
    DAO.getUser(req.query.username, function (foundUser) {
        if (foundUser || req.query.username.length < 4) {
            data = {
                username: req.query.username,
                success: true
            }
            res.send(data);
        } else if (req.query.username) {
            data = {
                username: req.query.username
            }
            res.send(data);
        }

    });

});

// -------------------------------------------------------------------------
// Helper functions
// -------------------------------------------------------------------------

function createArticleSamples(articles) {
    for (var i = 0; i < articles.length; i++) {
      let sample = articles[i].content.substring(0, 100); 
      articles[i].sample = sample.replace(/<[^>]*>/g, '');
    }
  }


// -------------------------------------------------------------------------
// Validate functions
// -------------------------------------------------------------------------

function validateDescription(description) {
    var descriptionRegex = new RegExp(/^[A-Za-z0-9!@#$%^&*()-_ ]{0,100}$/);
    if (descriptionRegex.test(description) != true) {
        console.log("description is greater than 100 characters");
        return false;
    } else {
        return true;

    }
}

function validateCountry(country) {
    var rawdata = fs.readFileSync('countries.json');
    var countries = JSON.parse(rawdata);
    for (var i = 0; i < countries.length; i++) {
        if (country == countries[i]) {
            return true;
        }
    }
    return false;
}

function validateDOB(dob) {

    var date = dob.split("-");

    if (isNaN(parseInt(date[0])) || parseInt(date[0]) > 2001 || parseInt(date[0]) < 1901) {
        console.log("year invalid " + date[0]);
        return false;
    }
    if (isNaN(parseInt(date[1])) || parseInt(date[1]) > 12 || parseInt(date[1]) < 1) {
        console.log("month invalid " + date[1]);
        return false;
    }
    if (isNaN(parseInt(date[2])) || parseInt(date[2]) > 31 || parseInt(date[2]) < 1) {
        console.log("date invalid " + date[2]);
        return false
    }

    return true;
}

function validateName(name) {
    var nameRegex = new RegExp(/^[A-Za-z]*$/gm);
    if (nameRegex.test(name) != true) {
        console.log("name contains invalid chracters");
        return false;
    } else {
        return true;

    }
}

function validateUsername(username) {
    var userRegex = new RegExp(/[A-Za-z0-9!@#$%^&*()-_\S]{4,12}/);
    if (userRegex.test(username) != true) {
        console.log("username is invalid");
        return false;
    } else {
        return true;
    }
}

function validatePassword(password) {
    var passRegex = new RegExp(/[A-Za-z0-9!@#$%^&*()-_\S]{4,12}/);
    if (passRegex.test(password) != true) {
        console.log("password is not 4-12 characters");
        return false;
    } else {
        return true;
    }
}

module.exports = profileRouter;