const express = require('express');
const articleRouter = express.Router();
const DAO = require('../dao-module');
const fs = require("fs");


// ------------------------------------------------
// FUNCTIONS 
// ------------------------------------------------

function checkCommentOwner(comments, loggedInUser) {
    for (var i = 0; i < comments.length; i++) {

        if (comments[i].username == loggedInUser.username) {
            comments[i]["isOwner"] = true;
        }

    }
}


// ------------------------------------------------
// Route Handlers  
// ------------------------------------------------


// When the user POSTs to "/add", add a new article, then redirect back to articles display.
articleRouter.post('/add', function (req, res) {

    if (req.isAuthenticated()) {
        var username = req.user.username;
    }

    var articleTitle = req.body.title;
    var articleContent = req.body.content;

    DAO.addArticle(articleTitle, articleContent, username, function (id) {

        console.log("New article added with id = " + id);

        res.redirect(`/article/${id}`); // go straight into the articleView to view full article
    });
});

articleRouter.post('/edit', function (req, res) {

    if (req.isAuthenticated()) {
        var username = req.user.username;
    }

    var articleID = req.body.id;
    var articleTitle = req.body.title;
    var articleContent = req.body.content;

    console.log(articleID);

    DAO.editArticle(articleTitle, articleContent, articleID, function (id) {

        console.log("Article " + id + "has been edited");

        res.redirect('back');
    });
});

articleRouter.get('/delete/:articleID', function (req, res) {

    var articleId = req.params.articleID;
    DAO.deleteArticle(articleId, function (rowsAffected) {

        console.log(rowsAffected + " row(s) affected.");

        if(req.query.profilePage) res.redirect('/profile');
        else res.redirect('/')

    });
});

articleRouter.get('/:articleID', function (req, res) {
    let articleID = req.params.articleID;
    let defaultAvatars = fs.readdirSync('./public/images/defaultAvatars/') ; 


    if (req.isAuthenticated()) { // Render view for logged in user

        DAO.getUser(req.user.username, function (loggedInUser) {

            DAO.getArticle(articleID, function (article) {

                DAO.getCommentsForArticle(articleID, function (comments) {

                    if (article.username == loggedInUser.username) loggedInUser.articleOwner = true;

                    checkCommentOwner(comments, loggedInUser);

                    var data = {
                        loggedInUser: loggedInUser,
                        article: article,
                        comments: comments,
                        defaultAvatars: defaultAvatars
                    };

                    res.render('articleView', data);

                });
            });

        });

    } else { // Render view for guest

        DAO.getArticle(articleID, function (article) {

            DAO.getCommentsForArticle(articleID, function (comments) {

                res.render('articleView', {
                    article: article,
                    comments: comments,
                    defaultAvatars: defaultAvatars
                })

            });
        });

    }


});

articleRouter.post('/addComment', (req, res) => {
    if (req.isAuthenticated()) {
      var username = req.user.username;
    }
  
    var comment = req.body.comment;
    var articleID = req.query.articleID;
  
    DAO.addComment(username, articleID, comment, function (callback) {
      res.redirect('/article/' + articleID);
    });
  
  });

  articleRouter.get('/deleteComment/:commentID', (req, res) => {
    DAO.deleteComment(req.params.commentID, function (rowsAffected) {
      console.log(rowsAffected + " row(s) affected.");
      res.redirect('back');
    });
  });

module.exports = articleRouter;